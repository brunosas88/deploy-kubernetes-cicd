# deploy-kubernetes-cicd
Criação de pipeline para deploy em cluster Google Cloud de uma 
aplicação-exemplo.

## Objetivo
Utilizar pipeline do Gitlab para criação e atualização automatizada 
de imagens de docker com aplicação para geração de um cluster 
kubernetes utilizando a plataforma da Google Cloud que opere sobre 
os mesmos.

### Ressalvas
O projeto aqui apresentado somente gera automaticamente as imagens no
Docker por falta de um cluster criado na Google Cloud, por isso esse
*stage* do arquivo **.gitlab-ci.yml** está comentado.

## Pré-Requisitos
1. Gerar cluster na Google Cloud;
2. Gerar máquina virtual para agir como Bastion Host para CI/CD;
3. Adicionar as variáveis de ambiente pertinentes no GitLab;
4. Adicionar serviços de secret (secrets.yml) e load balancer
(services-lb-con.yml) no Bastion Host para configuração de IP do frontend e das variáveis
de ambiente do banco de dados;
   * Adicionar IP externo fornecido pelo serviço do load balancer para
acesso da aplicação utilizando o arquivo **index.html** contido na pasta **frontend** modificando 
o arquivo javascript **js.js**, linha 8, parâmentro **url** que deve conter o valor **htt://ip_da_aplicação** dentro das aspas.



